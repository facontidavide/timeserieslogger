#include <future>
#include <iostream>
#include "filelogger.h"


FileLogger::FileLogger(const std::string &filename, const std::string& schema)
{
    _file = fopen(filename.c_str(), "wb");

    if( _file ){
        fwrite(schema.data(), 1,
               schema.size()*sizeof(char),
               _file);
    }

    LZ4_resetStream( &_lz4_stream );

    _buffer_index = 0;
}

FileLogger::~FileLogger()
{
    int index = _buffer_index;

    char comp_buffer[ LZ4_COMPRESSBOUND(BUFFER_SIZE) ];

    const int64_t comp_bytes = LZ4_compress_fast_continue(
                &_lz4_stream,                  // stream
                reinterpret_cast<char *>( _buffer[ index ] ) ,   // source
                comp_buffer,                // destination
                _buffer_offset,             // source size
                sizeof(comp_buffer),        //max output size
                1  // acceleration
                );

    fwrite( &comp_bytes,  sizeof(int64_t), 1, _file);
    fwrite( comp_buffer, 1, comp_bytes, _file);

    if( _file ){
        fclose( _file );
    }
}

void FileLogger::appendMessage(DynamicIntrospection::MessageRef msg )
{

    size_t chunk_size = msg.size + 4;
    bool buffer_full = (chunk_size + _buffer_offset) > BUFFER_SIZE;


    // compress and write to file in another thread
    if( buffer_full )
    {
        int index = _buffer_index;

        std::async( std::launch::async, [this, index, chunk_size]( )
        {
            char comp_buffer[ LZ4_COMPRESSBOUND(BUFFER_SIZE) ];

            int64_t comp_bytes = LZ4_compress_fast_continue(
                        &_lz4_stream,                // stream
                        reinterpret_cast<char *>( _buffer[ index ] )  ,   // source
                        comp_buffer,                // destination
                        _buffer_offset,             // source size
                        sizeof(comp_buffer),        //max output size
                        1  // acceleration
                        );

          //  std::cout << " compressing block " << comp_bytes << std::endl;
            fwrite( &comp_bytes,  sizeof(int64_t), 1, _file);
            fwrite( comp_buffer, 1, comp_bytes, _file);
        }
        );

        _buffer_index  = (_buffer_index +1 )%2;
        _buffer_offset = 0;
    }
    //-------------------------------------------------

    uint8_t* dst = &(_buffer[ _buffer_index ][_buffer_offset]);

    (*dst) =  msg.size & 0xFF;
    dst++;
    (*dst) =  ( msg.size >> 8) & 0xFF;
    dst++;
    (*dst) =  ( msg.size >> 16) & 0xFF;
    dst++;
    (*dst) =  ( msg.size >> 24) & 0xFF;
    dst++;

    for (uint32_t i=0; i< msg.size; i++)
    {
        *dst = msg.data[i];
        dst++;
    }
    _buffer_offset +=  msg.size + 4;
}
