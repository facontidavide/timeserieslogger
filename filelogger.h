#ifndef FILELOGGER_H
#define FILELOGGER_H

#include <string>
#include <vector>
#include <stdio.h>
#include "dynamiclogger.h"
#include "lz4.h"

//---------------------------------------------------
// helper class that open a file at construction and close it at destruction
class FileLogger
{
public:

    static const long BUFFER_SIZE = 1024*512 ; //0.5 MB

    FileLogger(const std::string& filename, const std::string& schema);
    ~FileLogger();

    void appendMessage(DynamicIntrospection::MessageRef msg);

private:
    uint8_t _buffer[2][BUFFER_SIZE];
    int      _buffer_index;
    int64_t _buffer_offset;
    FILE* _file;

    LZ4_stream_t _lz4_stream;
};

#endif // FILELOGGER_H
