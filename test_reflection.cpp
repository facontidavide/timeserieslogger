#include <iostream>
#include  <stdio.h>
#include "flatbuffers/reflection.h"
#include "flatbuffers/flatbuffers.h"
#include "flatbuffers/idl.h"
#include "flatbuffers/util.h"
#include "test_generated.h"

using namespace std;


void CreateBlankTable(flatbuffers::FlatBufferBuilder& fbb, const reflection::Schema& schema )
{
    fbb.Clear();

}


int main(int argc, char *argv[])
{

    std::string schemafile;

    bool ok = flatbuffers::LoadFile("test.fbs", false, &schemafile);
    if (!ok) {
        printf("couldn't load files!\n");
        return 1;
    }

    // parse schema first, so we can use it to parse the data after
    flatbuffers::Parser parser;

    ok = parser.Parse(schemafile.c_str(), nullptr, nullptr);
    assert(ok);

    // store the schema in parser.builder_
    parser.Serialize();

    // build reflection::Schema
    auto& schema = *reflection::GetSchema( parser.builder_.GetBufferPointer() );

    // take the root_table and its fields
    auto root_table = schema.root_table();

    if( root_table)
    {
        printf( "root_table: %s\n", root_table->name()->c_str() );
    }
    else{
        printf( "no root_table\n" );
    }

    for(int obj=0; obj< schema.objects()->size(); obj++)
    {
        auto object = schema.objects()->Get(obj);
        printf( "\nObject: %s\n", object->name()->c_str() );

        auto fields =  object->fields();
        for (int i=0; i< fields->size(); i++ )
        {
            auto field_ptr = fields->Get(i);
            const reflection::Field& field = *field_ptr;

            const char* type_name = EnumNameBaseType( field.type()->base_type() );

            if( field.type()->base_type() == reflection::Obj)
            {
                type_name = schema.objects()->Get( field.type()->index() )->name()->c_str();
            }

            printf("field %s: %s   %d\n",
                   field.name()->c_str(),
                   type_name,
                   field.offset() );
        }
    }


    //-----------------------------------


    /* for (int i=0; i< fbb.GetSize(); i++)
    {
        printf("%02X ", fbb.GetBufferPointer()[i] );
    }
    printf("\n");*/

    return 0;
}
