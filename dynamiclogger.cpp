#include "dynamiclogger.h"

#include <vector>
#include <memory>
#include <set>
#include <stdint.h>
#include "flatbuffers/reflection.h"
#include "flatbuffers/flatbuffers.h"
#include "flatbuffers/idl.h"
#include <iostream>
#include <functional>
#include <future>

using flatbuffers::voffset_t;
using flatbuffers::uoffset_t;


class DynamicIntrospection::PrivateImplementation{
public: 

    template <typename T>
    void registerBasicType(T* variable,
                           const std::string& id,
                           const char* type_name);

    template <typename T>
    void registerBasicVector(T* vector_ptr, size_t length,
                             const std::string& id,
                             reflection::BaseType reflection_type,
                             const char* type_name);

    std::vector< std::string >               registered_id;
    std::vector< std::function<void(flatbuffers::Table*, voffset_t)> >  funcSetField;
    std::vector< std::function<void(voffset_t, uoffset_t)> >  funcAddToBuilder;
    std::vector< std::function<uoffset_t()> >      funcCreateVector;

    std::vector< uint16_t >   field_offset;

    bool table_needs_to_be_updated;

    std::string schema_table_string;
    std::string schema_predefine;

    std::vector<uint8_t> raw_schema_buffer;
    flatbuffers::FlatBufferBuilder fbb;
    const reflection::Schema* schema;

    void generateFlatbufferSchema();
    void printSchema();

};


template <typename T>
void DynamicIntrospection::PrivateImplementation::registerBasicType(
        T* variable,
        const std::string& id,
        const char* type_name )
{
    table_needs_to_be_updated = true;

    schema_table_string.append( id + std::string(" : ") + type_name + std::string(";\n"));

    registered_id.push_back(id);
    field_offset.push_back(0); // placeholder

    //---------------------------------------------------
    // Hilario, I don't care that you don't like lambdas... it is just easier this way :P
    auto set_field =  [this, variable](flatbuffers::Table* table, voffset_t field_offset)
    {
        table->SetField( field_offset, *(variable) );
    };

    funcSetField.push_back( set_field );

    auto add_to_builder = [this](voffset_t field_offset, uoffset_t )
    {
        fbb.AddElement<T>(field_offset, 0);
    };

    funcAddToBuilder.push_back( add_to_builder );

    funcCreateVector.push_back( [](){ return 0; } ) ; // empty function. just added to keep consistent the index
}


template <typename T>
void DynamicIntrospection::PrivateImplementation::registerBasicVector(
        T* vector_ptr, size_t vector_size,
        const std::string& id,
        reflection::BaseType reflection_type,
        const char* type_name )
{
    table_needs_to_be_updated = true;

    schema_table_string.append( id + std::string(" : [") + type_name + std::string("];\n"));

    registered_id.push_back(id);
    field_offset.push_back(0); // placeholder

    //---------------------------------------------------
    // Hilario, I don't care that you don't like lambdas... it is just easier this way :P
    auto set_field =  [this, vector_ptr, vector_size, reflection_type]
            (flatbuffers::Table* table, voffset_t field_offset)
    {
        const auto& vect = table->GetPointer<flatbuffers::VectorOfAny *>( field_offset );
        for (int v=0; v < vector_size; v++)
        {
            flatbuffers::SetAnyVectorElemF( vect, reflection_type, v, vector_ptr[v]);
        }
    } ;

    funcSetField.push_back( set_field );

    auto add_value_to_builder = [this](voffset_t field_off, uoffset_t vector_off)
    {
        fbb.AddOffset<T>(field_off, vector_off);
    };
    funcAddToBuilder.push_back(add_value_to_builder);

    auto prepare_vector = [this, vector_ptr, vector_size ]()
    {
        return fbb.CreateVector( vector_ptr, vector_size ).o;
    };

    funcCreateVector.push_back(  prepare_vector );
}




DynamicIntrospection::~DynamicIntrospection()
{

}

void DynamicIntrospection::unregister(const std::string &id)
{
    printf("unregistering a pointer\n" );
}

DynamicIntrospection::DynamicIntrospection():
    pi( new struct DynamicIntrospection::PrivateImplementation )
{
    pi->table_needs_to_be_updated = true;
}

DynamicIntrospection& DynamicIntrospection::Instance()
{
    // this is safe in C++11 (not so much in older C++)
    // see http://preshing.com/20130930/double-checked-locking-is-fixed-in-cpp11/
    static DynamicIntrospection di;
    return di;
}

template <> void DynamicIntrospection::registerVariable(int *variable, const std::string& id)
{
    pi->registerBasicType(variable,id, "int");
}

template <> void DynamicIntrospection::registerVariable(unsigned *variable, const std::string& id)
{
    pi->registerBasicType(variable,id, "uint");
}

template <> void DynamicIntrospection::registerVariable(float *variable, const std::string& id)
{
    pi->registerBasicType(variable,id, "float");
}

template <> void DynamicIntrospection::registerVariable(double *variable, const std::string& id)
{
    pi->registerBasicType(variable,id, "double");
}

template <> void DynamicIntrospection::registerVariable(long *variable, const std::string& id)
{
    pi->registerBasicType(variable,id, "long");
}

template <> void DynamicIntrospection::registerVariable(short *variable, const std::string& id)
{
    pi->registerBasicType(variable,id, "short");
}
//-------------------
template <> void DynamicIntrospection::registerVector(int *data, size_t length, const std::string& id)
{
    pi->registerBasicVector(data, length, id, reflection::Int, "int");
}

template <> void DynamicIntrospection::registerVector(unsigned *data, size_t length, const std::string& id)
{
    pi->registerBasicVector(data, length, id, reflection::UInt, "uint");
}

template <> void DynamicIntrospection::registerVector(float *data, size_t length, const std::string& id)
{
    pi->registerBasicVector(data, length, id, reflection::Float, "float");
}

template <> void DynamicIntrospection::registerVector(double *data, size_t length, const std::string& id)
{
    pi->registerBasicVector(data, length, id, reflection::Double, "double");
}

template <> void DynamicIntrospection::registerVector(short *data, size_t length, const std::string& id)
{
    pi->registerBasicVector(data, length, id, reflection::Short, "short");
}

template <> void DynamicIntrospection::registerVector(long *data, size_t length, const std::string& id)
{
    pi->registerBasicVector(data, length, id, reflection::Long, "long");
}

//-------------------



DynamicIntrospection::MessageRef DynamicIntrospection::createMessage()
{
    if( pi->table_needs_to_be_updated )
    {
        pi->generateFlatbufferSchema();
        std::cout << std::endl;
        pi->printSchema();
        pi->table_needs_to_be_updated = false;
    }

    auto root_table = flatbuffers::GetAnyRoot( pi->fbb.GetBufferPointer() );

    for (int i=0; i< pi->funcSetField.size(); i++)
    {
        int offset = pi->field_offset[ i ];
        pi->funcSetField[i]( root_table, offset );
    }

    MessageRef msg = {pi->fbb.GetBufferPointer() ,  pi->fbb.GetSize()  };
    return msg;
}

std::string DynamicIntrospection::getSchema()
{
    std::string fbs;
    fbs.append( pi->schema_predefine );
    fbs.append( "table Data { \n" );
    fbs.append( pi->schema_table_string );
    fbs.append( "}\n\n root_type Data;\n" );
    return fbs;
}

void DynamicIntrospection::readAndPrintMessage(const DynamicIntrospection::MessageRef& msg)
{
    using namespace flatbuffers;

    auto schema_root = pi->schema->root_table();
    std::cout << "schema_root: " << schema_root->name()->c_str() << "\n";

    auto fields = schema_root->fields();

    const uint8_t* flatbuffer = (const uint8_t*) (msg.data);

    for (int c =0; c < msg.size; c++)
    {
        printf("%02d ", flatbuffer[c]);
    }
    std::cout << std::endl;

    auto root_table = GetAnyRoot( flatbuffer );

    for (int f=0; f< fields->size(); f++)
    {
        auto field = fields->Get(f);
        const char* field_name = field->name()->c_str();

       // std::cout << "field: "   << field_name  << " "<< field->type()->base_type() <<"  =  ";

        if( field->type()->base_type() == reflection::Vector)
        {
            const auto& vect = GetFieldAnyV( *root_table, *field );
            std::cout << "[" <<  vect->size() << "]  ";

            const auto& vect_type = field->type()->element();

            for (int v=0; v < vect->size(); v++)
            {
                double value = GetAnyVectorElemF( vect, vect_type, v);
                std::cout << value << " ";
            }
        }
        else{
            double value = GetAnyFieldF(*root_table, *field );
            std::cout <<  value ;
        }
        std::cout << std::endl;
    }
}




void DynamicIntrospection::PrivateImplementation::generateFlatbufferSchema()
{
    //----------------------------------
    // create the schema in text format

    std::string fbs;
    fbs.append( schema_predefine );
    fbs.append( "table Data { \n" );
    fbs.append( schema_table_string );
    fbs.append( "}\n\n root_type Data;\n" );

    //----------------------------------
    // parse the schema
    flatbuffers::Parser parser;
    bool ok = parser.Parse( fbs.c_str(), nullptr, nullptr );

    if( !ok ) {
        std::cout << parser.error_<< std::endl;
    }

    assert("Parse failed " && ok);

    parser.Serialize();

    auto data_ptr = parser.builder_.GetBufferPointer();
    auto length = parser.builder_.GetSize();

    raw_schema_buffer = std::vector<uint8_t>(  &data_ptr[0], &data_ptr[length] );


    schema = reflection::GetSchema( raw_schema_buffer.data() );
    //---------------------------------------
    // store the offsets in the right order.

    for( int i=0; i< registered_id.size(); i++ )
    {
        auto id = registered_id[i];
        auto field = schema->root_table()->fields()->LookupByKey( id.c_str() );
        field_offset[i] = field->offset();
    }

    //----------------------------------
    // build the builder
    fbb.Clear();

    std::vector<uoffset_t> vector_offsets( funcCreateVector.size() );
    for (int i=0; i< funcCreateVector.size(); i++ )
    {
        vector_offsets[i] = funcCreateVector[i](); // will return 0 if it is not a vector
    }

    uoffset_t  start = fbb.StartTable();

    for (int i=0; i< funcAddToBuilder.size(); i++ )
    {
        funcAddToBuilder[i]( field_offset[i], vector_offsets[i] );
    }

    auto table = fbb.EndTable(start, funcAddToBuilder.size() );
    fbb.Finish( flatbuffers::Offset<flatbuffers::Table>( table ) );

}

void DynamicIntrospection::PrivateImplementation::printSchema()
{
    auto root = schema->root_table();

    auto fields = root->fields();

    for (int f=0; f< fields->size(); f++)
    {
        auto field = fields->Get(f);
        const char* field_name = field->name()->c_str();
      /*  std::cout << "field: "   << field_name
                  << " offset: " << field->offset()
                  << " type: "   << EnumNameBaseType( field->type()->base_type() )
                  << "\n";*/
    }
}

