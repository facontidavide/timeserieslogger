#include <iostream>
#include "dynamiclogger.h"
#include "Eigen/Core"
#include "filelogger.h"

using namespace std;

class Foo{
public:
    double B ;
    long  C ;
    float D ;
    int   E ;
    short F ;
    double vect_A[5] ;

    Foo()
    {
        DynamicIntrospection& DI = DynamicIntrospection::Instance();

        static int count = 0;

        char name[100];

        sprintf(name, "a_vect_%02d", count);
        DI.registerVector( vect_A, 5,  name);

        sprintf(name, "B_double_%02d", count);
        DI.registerVariable( &B, name);

        sprintf(name, "C_long_%02d", count);
        DI.registerVariable( &C, name);

        sprintf(name, "D_float_%02d", count);
        DI.registerVariable( &D, name);

        sprintf(name, "E_inte_%02d", count);
        DI.registerVariable( &E, name);

        sprintf(name, "F_short_%02d", count);
        DI.registerVariable( &F, name);

        count++;
    }
};

int main(int argc, char *argv[])
{

    Foo foo[100] ;

    DynamicIntrospection& DI = DynamicIntrospection::Instance();
    DynamicIntrospection::MessageRef msg = DI.createMessage();

    FileLogger logger("data.dtl", DI.getSchema() );

    for (long i=0; i< 1000*100; i++)
    {
        for (int f=0; f< 100; f++)
        {
            foo[f].B = 10 +  0.010*(float)i;
            foo[f].C = 20 +  0.011*(float)i;
            foo[f].D = 30 +  0.012*(float)i;
            foo[f].E = 40 +  0.013*(float)i;
            foo[f].F = 50 +  0.014*(float)i;

            for (int a=0; a < 5; a++)
            {
                foo[f].vect_A[a] = cos( 0.0001*(float)i ) + a;
            }
        }
        msg = DI.createMessage();
        logger.appendMessage( msg );
    }

    return 0;
}
