#ifndef _DYNAMIC_INTROSPECTION_
#define _DYNAMIC_INTROSPECTION_

#include <memory>
#include <type_traits>
#include <vector>
#include <stdio.h>
#include <mutex>

class DynamicIntrospection{

public:


    typedef struct{
        const uint8_t* data;
        size_t size;
    } MessageRef;


    static DynamicIntrospection& Instance();


    virtual ~DynamicIntrospection();

    template<typename T> std::shared_ptr<T> variableFactory(const std::string& id);

    template <typename T > void registerVariable( T* variable, const std::string& id);

    template <typename T > void registerVector( T* data_ptr, size_t length, const std::string& id);

    void unregister(const std::string& id );


    MessageRef createMessage();

    std::string getSchema();

    void readAndPrintMessage(const MessageRef &msg);

private:

    DynamicIntrospection();

    class PrivateImplementation;
    std::unique_ptr<PrivateImplementation> pi;

};



//---------------------------------------------------


template<typename T>
std::shared_ptr<T> DynamicIntrospection::variableFactory(const std::string& id)
{
    T* variable_ptr = new T;

    registerVariable( variable_ptr, id );

    return std::shared_ptr<T> (variable_ptr,
                              [this,id](T* ptr) {
        this->unregister(id);
        delete ptr;
        ptr = nullptr;
    }
    );
}









#endif // DYNAMICLOGGER_H
